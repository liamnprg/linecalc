{-# LANGUAGE CPP #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Brick
  ( App(..)
  , AttrMap
  , AttrName
  , BrickEvent(..)
  , CursorLocation
  , EventM
  , Next
  , Next
  , Padding(..)
  , Widget
  , (<+>)
  , (<=>)
  , attrMap
  , continue
  , cursorLocationName
  , defaultMain
  , fill
  , halt
  , loc
  , on
  , padRight
  , padTop
  , showCursor
  , str
  , vLimit
  , withAttr
  , withBorderStyle
  )
import Brick.Widgets.Border (borderWithLabel, hBorder, vBorder)
import Brick.Widgets.Border.Style (unicode)
import Brick.Widgets.Center (center)
import qualified Brick.Widgets.Core as BWC
import qualified Brick.Widgets.Edit as BWE
import Data.Foldable
import qualified Data.Text as T
import qualified Data.Text.Zipper as DTZ
import qualified Graphics.Vty as V
import Stack

myEditor :: BWE.Editor T.Text Name
myEditor = BWE.editorText Edit (Just 1) T.empty

myEditorWidget :: State -> Widget Name
myEditorWidget s =
  BWE.renderEditor (\x -> BWC.vBox $ map BWC.txt x) True (edtr s)

data State =
  State
    { edtr :: BWE.Editor T.Text Name
    , isEdit :: Bool
    }

data Name
  = Edit
  | None
  deriving (Ord, Eq, Show)

-- Game state struct, event type, 
app :: App State () Name
app =
  App
    { appDraw = drawUI
    , appChooseCursor = selectOpOrCmd
    , appHandleEvent = handleEvent
    , appStartEvent = return
    , appAttrMap = const theMap
    }

--Drawing Functions
drawUI :: State -> [Widget Name]
drawUI s =
  [ withBorderStyle unicode $
    borderWithLabel (str "Hello!") $
    (drawResults s) <=> hBorder <=> (padTop Max $ drawCmdBar s) <+>
    vBorder <+> (center $ drawOpsBar s)
  ]

drawResults :: State -> Widget Name
drawResults s = str "Results"

drawCmdBar :: State -> Widget Name
drawCmdBar s = withAttr cmdBarAttr (str "cmd: " <+> myEditorWidget s)

drawOpsBar :: State -> Widget Name
drawOpsBar s = str "Right"

selectOpOrCmd :: State -> [CursorLocation Name] -> Maybe (CursorLocation Name)
selectOpOrCmd s names = findfn tofind
  where
    tofind =
      if isEdit s
        then Just Edit
        else Nothing
    findfn =
      (\x ->
         find
           (\cursorlocationname -> cursorLocationName cursorlocationname == x)
           names)

switchFocusedPanel :: State -> State
switchFocusedPanel s = s {isEdit = not $ isEdit s}

--event handling
handleEvent :: State -> BrickEvent Name () -> EventM Name (Next State)
--quit
handleEvent s (VtyEvent (V.EvKey (V.KEsc) [])) = halt s
handleEvent s (VtyEvent (V.EvKey (V.KChar '\t') [])) =
  continue $ switchFocusedPanel s
--send text to editor
handleEvent s (VtyEvent (ev))
  | isEdit s == True = do
    newedtr <- BWE.handleEditorEvent ev (edtr s)
    continue $ s {edtr = newedtr}
  | otherwise = undefined
--remove do here, move code to separate function
--idk lol
handleEvent s ev = undefined

--colors and attrs
--use editAttr
cmdBarAttr :: AttrName
cmdBarAttr = "cmdBarAttr"

theMap :: AttrMap
theMap = attrMap V.defAttr [(cmdBarAttr, V.white `on` V.black)]

--  , (foodAttr, V.red `on` V.red)
--  , (gameOverAttr, fg V.red `V.withStyle` V.bold)
--  ]
--main
main :: IO ()
main = do
  let startState = State {edtr = myEditor, isEdit = True}
  s' <- defaultMain app startState
  putStrLn "BYE"
