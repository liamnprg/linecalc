{-# LANGUAGE MultiParamTypeClasses #-}

module Stack
  ( Stack
  , push
  , pop
  , toSi
  , readOp
  , mul
  , Op(..)
  , StackItem
  ) where

import qualified Data.Char as Char
import qualified Numeric.LinearAlgebra as NLA

data StackItem
  = Reg Double
  | Mat (NLA.Matrix Prelude.Double)
  deriving (Show, Eq)

--  | Vec (Vector Prelude.Double)
data Op
  = Mul
  | Add
  | Sub
  | Div
  deriving (Show, Eq, Read)

type Stack = [StackItem]

--add fail cases
pop :: Stack -> (Stack, StackItem)
pop (x:xs) = (xs, x)

push :: Stack -> StackItem -> Stack
push s si = si : s

toSi :: Int -> StackItem
toSi i = Reg (fromIntegral i)

readOp :: [Char] -> Op
readOp (c:cs) = (read $ (Char.toUpper c) : cs) :: Op

--will get changed later to generic binary application function
mul :: Stack -> Stack
mul s1 = toAppend ++ s0
  where
    (s2, si1) = pop s1
    (s0, si2) = pop s2
    inneres = mulInner si1 si2
    toAppend =
      case inneres of
        Just x -> [x]
        Nothing -> [si1, si2]

mulInner :: StackItem -> StackItem -> Maybe StackItem
mulInner (Reg a) (Reg b) = Just $ Reg $ a * b
mulInner (Mat a) (Mat b) = Just $ Mat $ a `times` b
mulInner _ _ = Nothing
