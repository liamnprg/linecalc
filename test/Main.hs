import Control.Monad
import Data.Monoid
import Stack

import Test.HUnit
import Test.HUnit.Base

pushTest = s1 @=? [toSi 1]
  where
    s = []
    s1 = push s (toSi 1)

readOpTest =
  [ Mul @=? readOp "mul"
  , Add @=? readOp "add"
  , Sub @=? readOp "sub"
  , Div @=? readOp "div"
  ]

--add bad multiplication tests later
mulTest = mul [toSi 5, toSi 3] @=? [toSi 15]

main :: IO ()
main = do
  runTestTTAndExit $
    TestList
      ((map TestCase readOpTest) ++ [TestCase pushTest, TestCase mulTest])
